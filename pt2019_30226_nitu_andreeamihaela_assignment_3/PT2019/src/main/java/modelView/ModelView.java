package modelView;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

import generateTables.TableGenerator;
import mainView.View;

/**
 * 
 * @author Andrea Clasa ModelView este superclasa abstractă a următoarelor patru
 *         clase: CartiView, PersoaneView, ComandaView și ComandaClientView .
 *         Aici se definește un model concret pentru aceste ferestre care vor
 *         avea în comun: definirea și plasarea butoanelor, definirea tabelului,
 *         crearea listelor de TextField-uri specifice operațiilor care au mai
 *         multe câmpuri de completat (adaugarea și update), iar restul
 *         (ștergere și căutare) conținând un singur câmp comun și pentru
 *         primele două: id-ul.
 */
public abstract class ModelView extends View {
	public JButton adaugare = new JButton("Adaugare");
	public JButton update = new JButton("Update");
	public JButton delete = new JButton("Delete");
	public JButton find = new JButton("Cautare");

	public JTable tabel;
	protected JFrame jfFind, jfAdd, jfDel, jfUpdate;
	protected JPanel jFind, jAdd, jDel, jUpdate;
	protected JButton okAdd, okDel, okUpdate, okFind;
	protected JScrollPane js;
	protected ArrayList<JTextField> addList = new ArrayList();
	protected ArrayList<JTextField> updateList = new ArrayList();
	public JTextField idDel, idFind;

	public ModelView(String namePage, String imgPath, JTable table) {
		super(namePage, imgPath, null);
		placeButtons();
		putTable(table);
		createFrames();
		addListeners();
	}

	public int getMaxIdFromTable() {
		return Integer.parseInt(tabel.getValueAt(tabel.getRowCount() - 1, 0).toString());
	}

	private void designFrames() {
		Dimension d = new Dimension(1500, 100);
		Dimension d2 = new Dimension(200, 100);

		jfFind.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		jfAdd.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		jfDel.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		jfUpdate.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		jfFind.setSize(d2);
		jfAdd.setSize(d);
		jfDel.setSize(d2);
		jfUpdate.setSize(d);

		jFind.setBackground(Color.orange);
		jAdd.setBackground(Color.orange);
		jDel.setBackground(Color.orange);
		jUpdate.setBackground(Color.orange);

		Color c = new Color(99, 198, 247);
		okFind = this.buttonStyle(okFind, 20, c);
		okAdd = this.buttonStyle(okAdd, 20, c);
		okDel = this.buttonStyle(okDel, 20, c);
		okUpdate = this.buttonStyle(okUpdate, 20, c);
	}

	public void createFrames() {
		jfFind = new JFrame("Adaugare");
		jfAdd = new JFrame("Adaugare");
		jfDel = new JFrame("Stergere");
		jfUpdate = new JFrame("Update");

		jFind = new JPanel();
		jAdd = new JPanel();
		jDel = new JPanel();
		jUpdate = new JPanel();

		okFind = new JButton("ok");
		okAdd = new JButton("ok");
		okDel = new JButton("ok");
		okUpdate = new JButton("ok");

		designFrames();

		idDel = new JTextField(3); // toate au id-ul comun, deci punem pt cele 3 frame-uri
		idFind = new JTextField(3);
		JTextField idTf1 = new JTextField(3);
		JTextField idTf2 = new JTextField(3);

		jAdd.add(okAdd);
		jDel.add(okDel);
		jUpdate.add(okUpdate);
		jFind.add(okFind);

		addFields("id:", idTf1);
		updateFields("id:", idTf2);

		JPanel jp = this.getBox("id:", idDel); // la delete,update avem un singur camp.
		jDel.add(jp);

		JPanel jp2 = this.getBox("id:", idFind);
		jFind.add(jp2);

		jfFind.setContentPane(jFind);
		jfAdd.setContentPane(jAdd);
		jfDel.setContentPane(jDel);
		jfUpdate.setContentPane(jUpdate);
	}

	protected void addFields(String s, JTextField tf) {// de fiecare data cand vrem sa adaugam noi campuri
		addList.add(tf);
		JPanel jp = this.getBox(s, tf);
		jAdd.add(jp);
	}

	protected void updateFields(String s, JTextField tf) {
		updateList.add(tf);
		JPanel jp = this.getBox(s, tf);
		jUpdate.add(jp);
	}

	public void showMe() {
		jf.setVisible(true);
	}

	public void putTable(JTable tabel) {
		if (js != null)
			getScreen().remove(js);
		this.tabel = tabel;
		js = new JScrollPane(tabel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		js.setVisible(true);
		js.setBounds(300, 100, 800, 570);
		getScreen().add(js);
	}

	private void addListeners() {
		adaugare.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				jfAdd.setVisible(true);

			}

		});
		delete.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				jfDel.setVisible(true);

			}

		});
		update.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				jfUpdate.setVisible(true);

			}

		});
		find.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				jfFind.setVisible(true);

			}

		});
	}

	protected void placeButtons() {
		adaugare = buttonStyle(adaugare, 30, null);
		update = buttonStyle(update, 30, null);
		delete = buttonStyle(delete, 30, null);
		find = buttonStyle(find, 30, null);

		int space = 250;
		adaugare.setBounds(330, 30, 200, 50);
		update.setBounds(330 + space, 30, 200, 50);
		delete.setBounds(330 + 2 * space, 30, 200, 50);
		find.setBounds(330 + 3 * space, 30, 200, 50);

		screen.add(adaugare);
		screen.add(update);
		screen.add(delete);
		screen.add(find);

	}
}
