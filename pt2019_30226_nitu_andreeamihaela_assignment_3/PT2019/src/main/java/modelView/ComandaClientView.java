package modelView;

/**
 * Clasa ComandaClientView este o subclasă a clasei ModelView care are specifice ultilizarea unor funcții statice ale clasei Functions din pachetul business. Aceste funcții sunt specifice operațiilor cu comenzileClient.
 Ex. TableGenerator.getComandaClientTabel(); Functions.findComandaClient(id);

 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import business.Functions;
import generateTables.TableGenerator;
import model.Carte;
import model.ComandaClient;
import model.Persoana;

public class ComandaClientView extends ModelView {
	JTextField idClient = new JTextField(3);
	JTextField idClient2 = new JTextField(3);

	public ComandaClientView(JTable table) {
		super("Editarea tabelului cu comenziClient", "comandaClienti.jpg", table);
		addFields("idClient:", idClient);

		updateFields("idClient:", idClient2);
		buttonsListener();
	}

	public void refreshTable() throws SQLException {
		JTable tabel = TableGenerator.getComandaClientTabel();
		putTable(tabel);

	}

	public ComandaClient getAddInfo(ArrayList<JTextField> addList) {
		ComandaClient c = null;
		int id = 0, id_client = 0;

		try {
			id = Integer.parseInt(addList.get(0).getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "eroare parsare id");
			return null;
		}

		try {
			id_client = Integer.parseInt(addList.get(1).getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "eroare parsare id_client");
			return null;
		}

		return new ComandaClient(id, id_client);
	}

	public void buttonsListener() {
		okAdd.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				System.out.println("aiii");
				ComandaClient c = getAddInfo(addList);
				if (c != null) {
					JOptionPane.showMessageDialog(null, "adaugare realizata cu succes");
					Functions.adaugaComandaClientBd(c);
					try {
						refreshTable();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}

				}

			}

		});
		;
		okDel.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int i = -1;
				try {
					i = Integer.parseInt(idDel.getText());
				} catch (Exception ee) {
					JOptionPane.showInputDialog(null, "Eroare la parsare id");
				}
				if (i >= 0) {
					try {
						Functions.delComandaClient(i);
					} catch (SQLException e2) {
						e2.printStackTrace();
					}
					JOptionPane.showMessageDialog(null, "stergere realizata cu succes");

					try {
						refreshTable();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}

				}
			}

		}

		);
		;
		okUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ComandaClient p = getAddInfo(updateList);
				if (p != null) {
					JOptionPane.showMessageDialog(null, "update realizat cu succes");
					Functions.updateComandaClient(p.getId(), p);
					try {
						refreshTable();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
			}
		});
		okFind.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int i = -1;
				try {
					i = Integer.parseInt(idFind.getText());
				} catch (Exception ee) {
					JOptionPane.showInputDialog(null, "Eroare la parsare id");
					ee.printStackTrace();
				}
				if (i >= 0) {
					ComandaClient c = null;
					try {
						c = Functions.findComandaClient(i);
					} catch (Exception ex) {
						JOptionPane.showMessageDialog(null, "Nu se poate gasi acest id!");
					}
					if (c != null)
						JOptionPane.showMessageDialog(null, c.toString());

				}
			}
		}

		);
	}
}
