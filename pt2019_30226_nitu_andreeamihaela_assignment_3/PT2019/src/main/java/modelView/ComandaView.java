package modelView;

/**
 * Clasa ComandaView este o subclasă a clasei ModelView care are specifice ultilizarea unor funcții statice ale clasei Functions din pachetul business. Aceste funcții sunt specifice operațiilor cu comenzile.
 *Ex. TableGenerator.getComandaTabel(); Functions.findComanda (id);
 *
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import business.Functions;
import generateTables.TableGenerator;
import model.Carte;
import model.Comanda;
import model.ComandaClient;
import model.Persoana;

public class ComandaView extends ModelView {
	JTextField idCc = new JTextField(3);
	JTextField idCarte = new JTextField(3);
	JTextField nrBucati = new JTextField(3);

	JTextField idCc2 = new JTextField(3);
	JTextField idCarte2 = new JTextField(3);
	JTextField nrBucati2 = new JTextField(3);

	public ComandaView(JTable table) {
		super("Editarea tabelului cu comenzi", "comenzi.jpg", table);
		addFields("idComandaClient:", idCc);
		addFields("idCarte:", idCarte);
		addFields("nrBucati:", nrBucati);

		updateFields("idComandaClient:", idCc2);
		updateFields("idCarte:", idCarte2);
		updateFields("nrBucati:", nrBucati2);
		buttonsListener();
	}

	public void refreshTable() throws SQLException {
		JTable tabel = TableGenerator.getComandaTabel();
		putTable(tabel);

	}

	public Comanda getAddInfo(ArrayList<JTextField> addList) {
		int id = 0, id_ComandaClient = 0, id_carte = 0, nr_bucati = 0;

		try {
			id = Integer.parseInt(addList.get(0).getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "eroare parsare id");
			return null;
		}

		try {
			id_ComandaClient = Integer.parseInt(addList.get(1).getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "eroare parsare id_ComandaClient");
			return null;
		}

		try {
			id_carte = Integer.parseInt(addList.get(2).getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "eroare parsare id_carte");
			return null;
		}

		try {
			nr_bucati = Integer.parseInt(addList.get(3).getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "eroare parsare nr_bucati");
			return null;
		}

		return new Comanda(id, id_ComandaClient, id_carte, nr_bucati);
	}

	public void buttonsListener() {
		okAdd.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				Comanda c = getAddInfo(addList);
				if (c != null) {
					JOptionPane.showMessageDialog(null, "adaugare realizata cu succes");
					Functions.adaugaComandaBd(c);
					try {
						refreshTable();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}

			}

		});
		;
		okDel.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int i = -1;
				try {
					i = Integer.parseInt(idDel.getText());
				} catch (Exception ee) {
					JOptionPane.showInputDialog(null, "Eroare la parsare id");
				}
				if (i >= 0) {
					try {
						Functions.delComanda(i);
					} catch (SQLException e2) {
						e2.printStackTrace();
					}
					JOptionPane.showMessageDialog(null, "stergere realizata cu succes");

					try {
						refreshTable();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}

				}
			}
		}

		);

		okUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Comanda p = getAddInfo(updateList);
				if (p != null) {
					JOptionPane.showMessageDialog(null, "update realizat cu succes");
					Functions.updateComanda(p.getId(), p);
					try {
						refreshTable();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
			}
		});

		okFind.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int i = -1;
				try {
					i = Integer.parseInt(idFind.getText());
				} catch (Exception ee) {
					JOptionPane.showInputDialog(null, "Eroare la parsare id");
					ee.printStackTrace();
				}
				if (i >= 0) {
					Comanda c = null;
					try {
						c = Functions.findComanda(i);
					} catch (Exception ex) {
						JOptionPane.showMessageDialog(null, "Nu se poate gasi acest id!");
					}
					if (c != null)
						JOptionPane.showMessageDialog(null, c.toString());

				}
			}
		}

		);
		;
	}
}
