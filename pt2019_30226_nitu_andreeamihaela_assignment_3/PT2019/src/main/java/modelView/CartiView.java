package modelView;

/**
 * Clasa CartiView este o subclasă a clasei ModelView care are specifice ultilizarea unor funcții statice ale clasei Functions din pachetul business. Aceste funcții sunt specifice operațiilor cu cărți.
 Ex. TableGenerator.getCartiTabel(); Functions.findCarte(id);

 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import business.Functions;
import generateTables.TableGenerator;
import model.Carte;
import model.Persoana;

public class CartiView extends ModelView {
	JTextField titlu = new JTextField(10);
	JTextField gen = new JTextField(10);

	JTextField titlu2 = new JTextField(10);
	JTextField gen2 = new JTextField(10);

	JTextField pret = new JTextField(10);
	JTextField nr_exemplare = new JTextField(10);
	JTextField pret2 = new JTextField(10);
	JTextField nr_exemplare2 = new JTextField(10);

	public CartiView(JTable table) {
		super("Editarea tabelului cu carti", "Carti.jpg", table);
		addFields("titlu:", titlu);
		addFields("pret:", pret);
		addFields("nr_exemplare:", nr_exemplare);
		addFields("gen:", gen);

		updateFields("titlu:", titlu2);
		updateFields("pret:", pret2);
		updateFields("nr_exemplare:", nr_exemplare2);
		updateFields("gen:", gen2);
		buttonsListener();

	}

	public void refreshTable() throws SQLException {
		JTable tabel = TableGenerator.getCartiTabel();
		putTable(tabel);

	}

	public Carte getAddInfo(ArrayList<JTextField> addList) {
		Carte c = null;
		int id = 0, nr_exemplare = 0, pret = 0;

		try {
			id = Integer.parseInt(addList.get(0).getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "eroare parsare id");
			return null;
		}

		String titlu = "\"" + addList.get(1).getText() + "\"";

		try {
			pret = Integer.parseInt(addList.get(2).getText());
			;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "eroare parsare pret");
			return null;
		}

		try {
			nr_exemplare = Integer.parseInt(addList.get(3).getText());
			;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "eroare parsare nr_exemplare");
			return null;
		}
		String gen = "\"" + addList.get(4).getText() + "\"";
		return new Carte(id, titlu, gen, pret, nr_exemplare);
	}

	public void buttonsListener() {
		okAdd.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				Carte c = getAddInfo(addList);
				if (c != null) {
					JOptionPane.showMessageDialog(null, "adaugarea  nu se realizeaza");
					Functions.adaugaCarteBd(c);
					try {
						refreshTable();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}

				}

			}

		});
		;

		okDel.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int i = -1;
				try {
					i = Integer.parseInt(idDel.getText());
				} catch (Exception ee) {
					JOptionPane.showMessageDialog(null, "Eroare la parsare id");
				}
				if (i >= 0) {
					try {
						Functions.delCarte(i);
					} catch (SQLException e2) {
						e2.printStackTrace();
					}
					JOptionPane.showMessageDialog(null, "stergere realizata cu succes");

					try {
						refreshTable();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}

				}
			}
		}

		);
		okUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Carte p = getAddInfo(updateList);
				if (p != null) {
					JOptionPane.showMessageDialog(null, "update realizat cu succes");
					Functions.updateCarte(p.getId(), p);
					try {
						refreshTable();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
			}
		});

		okFind.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int i = -1;
				try {
					i = Integer.parseInt(idFind.getText());
				} catch (Exception ee) {
					JOptionPane.showInputDialog(null, "Eroare la parsare id");
					ee.printStackTrace();
				}
				if (i >= 0) {
					Carte c = null;
					try {
						c = Functions.findCarte(i);
					} catch (Exception ex) {
						JOptionPane.showMessageDialog(null, "Nu se poate gasi acest id!");
					}
					if (c != null)
						JOptionPane.showMessageDialog(null, c.toString());

				}
			}
		}

		);
	}
}
