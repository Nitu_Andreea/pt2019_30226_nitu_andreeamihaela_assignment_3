package modelView;

import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import business.Functions;
import generateTables.TableGenerator;
import model.Carte;
import model.Persoana;

/**
 * 
 * @author Andrea Clasa PersoaneView este o subclasă a clasei ModelView care are
 *         specifice ultilizarea unor funcții statice ale clasei Functions din
 *         pachetul business. Aceste funcții sunt specifice operațiilor cu
 *         persoanele din baza de date. Ex. TableGenerator.getPersoanaTabel();
 *         Functions.findPersoana(id);
 * 
 */
public class PersoaneView extends ModelView {
	JTextField nume = new JTextField(10);
	JTextField telefon = new JTextField(10);
	JTextField strada = new JTextField(10);

	JTextField nume2 = new JTextField(10);
	JTextField telefon2 = new JTextField(10);
	JTextField strada2 = new JTextField(10);

	public PersoaneView(JTable table) {
		super("Editarea tabelului cu persoane", "clienti.jpg", table);
		addFields("nume:", nume);
		addFields("telefon:", telefon);
		addFields("strada:", strada);

		updateFields("nume:", nume2);
		updateFields("telefon:", telefon2);
		updateFields("strada:", strada2);
		buttonsListener();

	}

	public void refreshTable() throws SQLException {
		JTable tabel = TableGenerator.getPersoaneTabel();
		putTable(tabel);

	}

	public Persoana getAddInfo(ArrayList<JTextField> addList) {
		Persoana c = null;
		int id = 0;

		try {
			id = Integer.parseInt(addList.get(0).getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "eroare parsare id");
			return null;
		}

		String nnume = "\"" + addList.get(1).getText() + "\"";
		String ttelefon = "\"" + addList.get(2).getText() + "\"";
		String sstrada = "\"" + addList.get(3).getText() + "\"";

		return new Persoana(id, nnume, ttelefon, sstrada);
	}

	public void buttonsListener() {
		okAdd.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				System.out.println("aiii");
				Persoana p = getAddInfo(addList);
				if (p != null) {
					JOptionPane.showMessageDialog(null, "adaugare realizata cu succes");
					Functions.adaugaPersoanaBd(p);
					try {
						refreshTable();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}

			}

		});
		;

		okUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Persoana p = getAddInfo(updateList);
				if (p != null) {
					JOptionPane.showMessageDialog(null, "update realizat cu succes");
					Functions.updatePersoana(p.getId(), p);
					try {
						refreshTable();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
			}
		});

		okDel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = -1;
				try {
					i = Integer.parseInt(idDel.getText());
				} catch (Exception ee) {
					JOptionPane.showInputDialog(null, "Eroare la parsare id");
				}
				if (i >= 0) {
					try {
						Functions.delPersoana(i);
					} catch (SQLException e2) {
						e2.printStackTrace();
					}
					JOptionPane.showMessageDialog(null, "stergere realizata cu succes");

					try {
						refreshTable();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}

				}
			}

		}

		);
		okFind.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int i = -1;
				try {
					i = Integer.parseInt(idFind.getText());
				} catch (Exception ee) {
					JOptionPane.showInputDialog(null, "Eroare la parsare id");
					ee.printStackTrace();
				}
				if (i >= 0) {
					Persoana c = null;
					try {
						c = Functions.findPersoana(i);
					} catch (Exception ex) {
						JOptionPane.showMessageDialog(null, "Nu se poate gasi acest id!");
					}
					if (c != null)
						JOptionPane.showMessageDialog(null, c.toString());

				}
			}
		}

		);
	}
}
