package model;

public class Persoana {
	private int id;
	private String nume;
	private String telefon;
	private String strada;

	public Persoana(int id, String nume, String telefon, String strada) {
		this.id = id;
		this.nume = nume;
		this.telefon = telefon;
		this.strada = strada;
	}

	public Persoana() {
	}
	public String toString() {
		return "id= "+id+"; "+"nume= "+nume+"; "+"telefon= "+telefon+"; "+"strada= "+strada+"; ";
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getStrada() {
		return strada;
	}

	public void setStrada(String strada) {
		this.strada = strada;
	}

}
