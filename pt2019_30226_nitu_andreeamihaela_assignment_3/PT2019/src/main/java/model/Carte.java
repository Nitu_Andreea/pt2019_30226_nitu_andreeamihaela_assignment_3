package model;

public class Carte {
	private int id;
	private String titlu;
	private int pret;
	private int nr_exemplare;
	private String gen;

	public Carte(int id, String titlu, String gen,int pret,int nr_exemplare) {
		this.setPret(pret);
		this.nr_exemplare=nr_exemplare;
		this.id = id;
		this.titlu = titlu;
		this.gen = gen;
	}
	public Carte() {
	}
	
	public String toString() {
		return "id="+id+"; "+"titlu="+titlu+"; "+ "pret= "+pret+"; " +"\n nr_exemplare="+nr_exemplare+"; "+"gen="+gen;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitlu() {
		return titlu;
	}
	public void setTitlu(String titlu) {
		this.titlu = titlu;
	}
	public String getGen() {
		return gen;
	}
	public void setGen(String gen) {
		this.gen = gen;
	}

	public int getNr_exemplare() {
		return nr_exemplare;
	}
	public void setNr_exemplare(int nr_exemplare) {
		this.nr_exemplare = nr_exemplare;
	}
	public int getPret() {
		return pret;
	}
	public void setPret(int pret) {
		this.pret = pret;
	}
	
	
}
