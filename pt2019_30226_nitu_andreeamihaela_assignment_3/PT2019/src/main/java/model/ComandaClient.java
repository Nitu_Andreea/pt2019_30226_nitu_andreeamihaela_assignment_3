package model;

public class ComandaClient {
	private int id,id_client;

	public ComandaClient(int id, int id_client) {
		super();
		this.id = id;
		this.id_client = id_client;
	}

	public ComandaClient() {
	
	}
	public String toString() {
		return "id= "+id+"; "+"id_client= "+id_client+"; ";
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_client() {
		return id_client;
	}

	public void setId_client(int id_client) {
		this.id_client = id_client;
	}
}
