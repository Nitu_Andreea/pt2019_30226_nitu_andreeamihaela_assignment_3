package model;

public class Comanda {
	private int id, id_ComandaClient, id_carte, nr_bucati;

	public Comanda(int id, int id_ComandaClient, int id_carte, int nr_bucati) {
		this.id = id;
		this.id_ComandaClient = id_ComandaClient;
		this.id_carte = id_carte;
		this.nr_bucati = nr_bucati;
	}
	
	public Comanda() {
		}
	
	public String toString() {
		return "id="+id+"; "+"id_ComandaClient="+id_ComandaClient+"; "+"id_carte="+id_carte+"; "+"nr_bucati="+nr_bucati;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_ComandaClient() {
		return id_ComandaClient;
	}

	public void setId_ComandaClient(int id_ComandaClient) {
		this.id_ComandaClient = id_ComandaClient;
	}

	public int getId_carte() {
		return id_carte;
	}

	public void setId_carte(int id_carte) {
		this.id_carte = id_carte;
	}

	public int getNr_bucati() {
		return nr_bucati;
	}

	public void setNr_bucati(int nr_bucati) {
		this.nr_bucati = nr_bucati;
	}

}
