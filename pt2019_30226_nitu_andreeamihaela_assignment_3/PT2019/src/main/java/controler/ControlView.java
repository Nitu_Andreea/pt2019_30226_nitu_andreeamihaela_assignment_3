package controler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JTable;

import business.Functions;
import generateTables.TableGenerator;
import mainView.*;
import model.Comanda;
import model.ComandaClient;
import modelView.*;
import secondaryView.*;

/**
 * 
 * @author Andrea Clasa ControlView este practic creierul execuției programului.
 *         Aici se întâlnesc toate frame-urile și interacționează. Aici se
 *         setează ce se întâmplă când utilizatorul apară pe diferite butoane,
 *         dar se și inițializează frame-urile și componentele vizuale.
 */
public class ControlView {
	AdminView adminView;
	MainView mainView;
	UserView userView;
	LogIn logIn;
	FacturaView facturaView;
	JTable tabel;

	PersoaneView persoaneView;
	CartiView cartiView;
	ComandaClientView comandaClientView;
	ComandaView comandaView;

	public ControlView() throws SQLException {

		createAllPages();
		userView.catalog.setVisible(false);

		addControlMainView();
		addControlAdminView();
		addControlUserView();

		addControlFacturaView();

	}

	private void createAllPages() throws SQLException {

		mainView = new MainView();
		adminView = new AdminView();

		try {
			userView = new UserView(TableGenerator.createTableCatalog());
			persoaneView = new PersoaneView(TableGenerator.getPersoaneTabel());// aici se pierde

			cartiView = new CartiView(TableGenerator.getCartiTabel());

			comandaClientView = new ComandaClientView(TableGenerator.getComandaClientTabel());
			comandaView = new ComandaView(TableGenerator.getComandaTabel());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "err");
		}

		logIn = new LogIn();
		facturaView = new FacturaView();
	}

	private void addControlMainView() {
		mainView.appearOnScreen();
		mainView.admin.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				adminView.appearOnScreen();
			}

		});

		mainView.user.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				userView.appearOnScreen();
			}

		});
	}

	private void addControlAdminView() {
		adminView.produse.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				cartiView.showMe();
			}

		});
		adminView.clienti.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				persoaneView.showMe();
			}

		});
		adminView.comanda.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				comandaView.showMe();
			}

		});
		adminView.comandaClient.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				comandaClientView.showMe();
			}

		});
	}

	public void addControlFacturaView() {
		facturaView.getFinish().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// valideaza existenta clientului in baza de date
				if (logIn.persoana != null) {

					int idMaxCom = comandaView.getMaxIdFromTable();
					int idMaxComandaClient = comandaClientView.getMaxIdFromTable();
					System.out.println(idMaxCom + " " + idMaxComandaClient + " " + logIn.persoana.getId());
					// initial intregistram clientul cu comanda
					Functions.adaugaComandaClientBd(new ComandaClient(idMaxComandaClient + 1, logIn.persoana.getId()));
					ArrayList<Comanda> list = userView.getComanda(idMaxCom, idMaxComandaClient + 1, logIn.persoana);
					boolean isOk = Functions.executaComenzile(list);
					if (isOk) {
						try {
							comandaView.refreshTable();
							comandaClientView.refreshTable();
							userView.refreshTable();
							cartiView.refreshTable();
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						JOptionPane.showMessageDialog(null,
								"Comanda finalizata.Produsele vor ajunge in maxim 3 zile. Cont curent");
						facturaView.getJf().setVisible(false);
						userView.catalog.setVisible(false);
						logIn.reset();
					} else
						JOptionPane.showMessageDialog(null, "Comanda nerealizata!");

				} else {
					JOptionPane.showMessageDialog(null, "Log-in obligatoriu!");

				}
			}
		});
	}

	private void addControlUserView() {
		userView.ok.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				JTable table = userView.getComandTable();
				facturaView.scrieFactura(userView.recordOrder());
				facturaView.appearOnScreen();
			}

		});

		userView.logIn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				logIn.reset();
				logIn.appearOnScreen();
			}

		});

		userView.catalogBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				userView.swapVisibilityCatalog();
			}

		});

	}
}
