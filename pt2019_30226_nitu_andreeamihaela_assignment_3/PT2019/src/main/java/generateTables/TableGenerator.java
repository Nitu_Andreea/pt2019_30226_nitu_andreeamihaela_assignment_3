package generateTables;

import java.awt.Color;
import java.awt.Font;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import business.Functions;
import daoModel.ComandaClientDao;
import model.Carte;

public class TableGenerator {

	private static JTable designTable(JTable table) {
		Font font = new Font("Monotype Corsiva", 1, 22);

		table.setFont(font);
		table.getTableHeader().setFont(new Font("Monotype Corsiva", 1, 26));
		table.setRowHeight(60);
		table.setBackground(Color.WHITE);
		table.setForeground(Color.black);
		
		table.getColumnModel().getColumn(0).setMinWidth(60);
		for(int i=1;i<table.getColumnCount();i++)
		table.getColumnModel().getColumn(i).setMinWidth(150);
		
		return table;
	}

	public static JTable createTableCatalog() throws SQLException {

		ArrayList<String> str = Functions.getFieldsCarte();
		str.add("Cumpar");

		ArrayList<String> list = Functions.getAllSelectedCarti();
		
		DefaultTableModel model = new DefaultTableModel();
		model.setColumnIdentifiers(str.toArray());

		JTable table = new JTable() {
			public boolean isCellEditable(int row, int column) {
				return column == 5;
			};
		};
		table.setModel(model);

		//table = designTable(table);
		
		
		
		
		JTableHeader th = table.getTableHeader();
		TableColumnModel tcm = th.getColumnModel();
		for (int i = 0; i < str.size(); i++) {
			TableColumn tc = tcm.getColumn(i);
			tc.setHeaderValue(str.get(i));
		}

		String info[] = new String[str.size()];
		int i;
		Iterator<String> s = list.iterator();
		while (s.hasNext()) {
			for (i = 0; i < str.size() - 1; i++) {
				info[i] = (String) s.next();
			}
			info[i] = "0";
			model.addRow(info);
		}
		Font font = new Font("Monotype Corsiva", 1, 22);

		table.setFont(font);
		table.getTableHeader().setFont(new Font("Monotype Corsiva", 1, 26));
		table.setRowHeight(60);
		table.setBackground(Color.WHITE);
		table.setForeground(Color.black);
		//aliniament diferit
		for(i=1;i<table.getColumnCount();i++)
			table.getColumnModel().getColumn(i).setMinWidth(150);
		
		//table.getColumn(3).setHeaderValue("pret");
	//	table.getColumn(4).setHeaderValue("buc");
		//table.getColumn(5).setHeaderValue("buy");
		
		table.getColumnModel().getColumn(0).setMinWidth(30);
		table.getColumnModel().getColumn(3).setMinWidth(30);
		table.getColumnModel().getColumn(4).setMinWidth(30);
		table.getColumnModel().getColumn(5).setMinWidth(30);
		//System.out.println("aici");
		return table;

	}


	private static JTable makeTable(ArrayList<String>str,ArrayList<String>list) {
		DefaultTableModel model = new DefaultTableModel();
		model.setColumnIdentifiers(str.toArray());
		
		JTable table = new JTable() {
			public boolean isCellEditable(int row, int column) {
				return false;
			};
		};
		table.setModel(model);
	
		
		table = designTable(table);

		JTableHeader th = table.getTableHeader();
		TableColumnModel tcm = th.getColumnModel();
		for (int i = 0; i < str.size(); i++) {
			TableColumn tc = tcm.getColumn(i);
			tc.setHeaderValue(str.get(i));
		}
	
		String info[] = new String[str.size()];
		int i;
		Iterator<String> s = list.iterator();
		while (s.hasNext()) {
			for (i = 0; i < str.size(); i++) {
				info[i] = (String) s.next();
			}
			//for (i = 0; i < str.size(); i++) {
		//	System.out.print(info[i]+" ");}
			//System.out.print("\n ");
			model.addRow(info);
		}
	
		return table;
	}
	public static JTable getComandaClientTabel() throws SQLException {

		ArrayList<String> str = Functions.getFieldsComandaClient();
		ArrayList<String> list = Functions.getAllSelectedComandaClient();
		return makeTable(str,list);
}

	public static JTable getCartiTabel() throws SQLException {
		ArrayList<String> str = Functions.getFieldsCarte();
		ArrayList<String> list = Functions.getAllSelectedCarti();
		return makeTable(str,list);
}


	public static JTable getPersoaneTabel() throws SQLException {
		ArrayList<String> str = Functions.getFieldsPersoana();
		ArrayList<String> list = Functions.getAllSelectedPersoane();
		return makeTable(str,list);
	}

	public static JTable getComandaTabel() throws SQLException {
		ArrayList<String> str = Functions.getFieldsComanda();
		ArrayList<String> list = Functions.getAllSelectedComanda();
		return makeTable(str,list);
	}
}
