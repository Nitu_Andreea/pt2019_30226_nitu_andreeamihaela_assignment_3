package mainView;

import java.awt.Color;

import javax.swing.JButton;

/**
 * 
 * @author Andrea Clasa AdminView este o pagină intermediară de a ajunge la
 *         funcțiile accesibile administratorului de magazin. Intermediară
 *         pentru că aceasta conține un meniu de selectare a zonei de lucru în
 *         funcție de tabelul pe care vrem să facem procesările. Aici se face
 *         trimiterea către paginile din pachetul modelView.
 */
public class AdminView extends View {
	public JButton produse = new JButton("Produse");
	public JButton clienti = new JButton("Clienti");
	public JButton comandaClient = new JButton("ComandaClient");
	public JButton comanda = new JButton("Comanda");

	public AdminView() {
		super("Admin page", "1.jpg", null);
		placeButtons();
	}

	private void placeButtons() {
		int size = 40;
		Color c = new Color(99, 198, 247);
		produse = buttonStyle(produse, size, c); // plasam cele 2 butoane si le realizam design-ul prin superclasa
		clienti = buttonStyle(clienti, size, c);
		comandaClient = buttonStyle(comandaClient, size, c);
		comanda = buttonStyle(comanda, size, c);
		int x = 770;
		int y = 400;
		int spaceY = 100;

		produse.setBounds(x, y - spaceY, 400, 50);
		clienti.setBounds(x, y, 400, 50);
		comandaClient.setBounds(x, y + spaceY, 400, 50);
		comanda.setBounds(x, y + 2 * spaceY, 400, 50);

		getScreen().add(produse);
		getScreen().add(clienti);
		getScreen().add(comandaClient);
		getScreen().add(comanda);
	}

}
