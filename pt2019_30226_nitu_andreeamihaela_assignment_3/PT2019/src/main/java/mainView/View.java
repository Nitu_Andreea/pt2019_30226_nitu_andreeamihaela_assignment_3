package mainView;

import java.awt.*;
import javax.swing.*;

/**
 * 
 * @author Andrea Clasa View este o dezvoltare a celei folosite în proiectele
 *         anterioare. Acum am mai adăugat funcții pentru afișarea tabelului în
 *         concordanță cu un JScrollPane. De asemenea, am mai adăugat alte
 *         funcții specifice cum ar fi appear/dissapear OnScreen și frame style.
 */
public abstract class View {
	protected Dimension d = new Dimension(1400, 730);
	protected Font myFont = new Font("Yu Mincho Demibold", 1, 22);
	protected Color color = Color.white;
	protected Color textColor = Color.black;
	protected ImageIcon img = new ImageIcon("img.png");
	protected JFrame jf;
	protected JLabel screen = new JLabel();

	public void putTable(JTable tabel) {
		JScrollPane js = new JScrollPane(tabel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		js.setVisible(true);
		js.setBounds(400, 100, 500, 600);
		getScreen().add(js);
	}

	public View() {

	}

	public View(String frameName, String imgPath, Dimension d) {
		if (d != null)
			this.d = d;
		this.img = new ImageIcon(imgPath); // setez imaginea specifica fiecarui frame
		fixFrame(frameName);
	}

	public JFrame fixFrame(String text) { // RUTINA DE INITIALIZARE A FERESTREI:
		setJf(new JFrame(text)); // dimensiune, imagine de fundal, culoare de fundal, vizibilitate
		setJf(new JFrame(text));
		getJf().setSize(d);

		getJf().setContentPane(getScreen());
		getScreen().setIcon(img);

		getJf().setVisible(false);
		getJf().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // si inchidere partiala a programului
		return getJf();
	}

	// METODE SPECIFICE DE DESIGN PENTRU PROIECT
	public JPanel getBox(String s, JTextField o) { // RETURNEAZA O COMPONENTA JPANEL, unind un textField si eticheta sa
		o = frameStyle(o);
		JLabel jl = new JLabel(s);
		o.setFont(myFont);
		jl.setFont(myFont);
		JPanel jp = new JPanel();
		jp.setBackground(new Color(249, 231, 159));
		jp.add(jl);
		jp.add((Component) o);
		jp.setSize(30, 60);
		return jp;
	}

	public JPanel getBox2(String s, JTextField o, JLabel waitCasa) {
		JPanel jp = getBox(s, o);
		JLabel jl2 = waitCasa;
		jp.add(jl2);
		jp.setSize(30, 60);
		return jp;
	}

	public JTextField frameStyle(JTextField idTf) {
		idTf.setFont(myFont);
		return idTf;

	}

	public JButton buttonStyle(JButton btn, int size, Color c) { // DESIGN BUTTON
		JButton jb = btn;
		if (c == null)
			jb.setBackground(color);
		else
			jb.setBackground(c);

		jb.setFont(new Font("Yu Mincho Demibold", 1, size));
		jb.setForeground(textColor);
		return jb;
	}

	public JButton buttonStyle(JButton btn) { // DESIGN BUTTON
		JButton jb = btn;
		jb.setBackground(color);
		jb.setFont(myFont);
		jb.setForeground(textColor);
		return jb;
	}

	public void appearOnScreen() {
		getJf().setVisible(true);
	}

	public void disappearOnScreen() {
		getJf().setVisible(false);
	}

	public JFrame getJf() {
		return jf;
	}

	public void setJf(JFrame jf) {
		this.jf = jf;
	}

	public JLabel getScreen() {
		return screen;
	}

	public void setScreen(JLabel screen) {
		this.screen = screen;
	}

}
