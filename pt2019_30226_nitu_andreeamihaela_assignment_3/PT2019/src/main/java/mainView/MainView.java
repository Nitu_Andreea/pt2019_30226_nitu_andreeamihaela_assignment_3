package mainView;

import javax.swing.*;

/**
 * 
 * @author Andrea Clasa MainView reprezintă prima fereastră unde practic avem
 *         numele magazinului ”CDSM”, un nume care reprezintă o provocare pentru
 *         utilizator să îl ghicească. Hint: se află printre cele 29 de clase,
 *         undeva în cod. Glumesc, apare complet pe o pagină în proiect. Aici
 *         avem cele două butoane care fac trimitere la zona de administrare
 *         respectiv lansare de comandă online.
 * 
 */
public class MainView extends View {
	public JButton admin = new JButton("Administrare");
	public JButton user = new JButton("Comanda online");

	public MainView() {
		super("Pagina principala", "00.jpg", null);
		placeButtons();
		getJf().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void placeButtons() {
		int size = 40;
		admin = buttonStyle(admin, size, null); // plasam cele 2 butoane si le realizam design-ul prin superclasa
		user = buttonStyle(user, size, null);
		int x = 470;
		int y = 300;
		int space = 100;
		admin.setBounds(x, y, 400, 50);
		user.setBounds(x, y + space, 400, 50);
		getScreen().add(admin);
		getScreen().add(user);
	}

}
