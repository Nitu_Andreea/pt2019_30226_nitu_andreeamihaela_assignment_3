package mainView;

import java.awt.Color;
import java.awt.Dimension;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import business.Functions;
import generateTables.TableGenerator;
import model.Comanda;
import model.Persoana;

/**
 * 
 * @author Andrea Clasa UserView prezintă o procesare de comandă care este
 *         realizată doar dacă sunt respectate anumite condiții: 
 *         1.Utilizatorul
 *         trebuie să realizeze un log-in corect -în cazul în care nu este
 *         întregistrat în baza de date, trebuie să se adreseze
 *         administratorului pentru a se înscrie. 
 *         2.Produsele comandate nu
 *         trebuie să depășească cantitatea valabilă în magazin, fapt pentru
 *         care, alegerea produselor se realizează prin completarea tabelului de
 *         produse, la care s-a adăugat câmpul „Cumpar”. Numărul de bucăți este
 *         afișat pentru fiecare produs. În cazul în care nu se ține cont de
 *         această constrângere se va afișa un mesaj de eroare prin care se
 *         anulează comanda. 
 *         3.Comanda este finalizată abia atunci când se apasă
 *         butonul de finalizare a comenzii, fapt care va determina schimbări în
 *         baza de date și va declanșa reactualizarea tabelului.
 * 
 */
public class UserView extends View {
	Color c = new Color(241, 196, 15);
	public JButton logIn = new JButton("Log in");
	public JButton catalogBtn = new JButton("Catalog");
	public JPanel catalog = new JPanel();
	public JTable tabel;
	public JButton ok = new JButton("Continua comanda");
	JScrollPane js;

	public UserView(JTable tabel) {
		super("User page", "2.jpg", null);
		placeButtons();
		this.tabel = tabel;
		putTable(tabel);
		placeCatalog();
	}

	public void putTable(JTable tabel) {
		if (js != null)
			getScreen().remove(js);
		this.tabel = tabel;
		js = new JScrollPane(tabel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		js.setVisible(true);
		js.setBounds(700, 10, 650, 670);
		getScreen().add(js);
	}

	private void placeCatalog() {

		// TABEL PUNE ZERO LA NR BUCATI!
		setInfoTabel();
		ok.setVisible(false);
		js.setVisible(false);
	}

	private void setInfoTabel() {
		// tabel.setEnabled(false);
		int rows = tabel.getRowCount();
		int cols = tabel.getColumnCount();
		System.out.println(rows + " " + cols);
		for (int i = 0; i < rows; i++)
			tabel.setValueAt(0, i, cols - 1);
	}

	private void placeButtons() {
		int size = 40;
		logIn = buttonStyle(logIn, size, c); // plasam cele 2 butoane si le realizam design-ul prin superclasa
		catalogBtn = buttonStyle(catalogBtn, size, c);
		ok = buttonStyle(ok, size, new Color(99, 198, 247));

		int x = 220;
		int y = 400;
		int space = 100;

		logIn.setBounds(x, y, 400, 50);
		catalogBtn.setBounds(x, y + space, 400, 50);
		ok.setBounds(x, y + 2 * space, 400, 50);

		getScreen().add(logIn);
		getScreen().add(catalogBtn);
		getScreen().add(ok);
	}

	public void swapVisibilityCatalog() {
		if (ok.isVisible()) {
			ok.setVisible(false);
			js.setVisible(false);
		} else {
			ok.setVisible(true);
			js.setVisible(true);
		}

	}

	public JTable getComandTable() {
		return tabel;
	}

	public int getMaxIdFromTable() {
		return Integer.parseInt(tabel.getValueAt(tabel.getRowCount(), 0).toString());
	}

	public ArrayList<Comanda> getComanda(int idMax, int idComClient, Persoana p) {

		// Trebuie sa gasim id_ComandaClient a persoanei p
		// System.out.println("id com:"+id_ComandaClient);
		ArrayList<Comanda> list = new ArrayList<Comanda>();

		int i, j;

		// caut obiectele
		int id = idMax + 1;
		int rows = tabel.getRowCount();
		int cols = tabel.getColumnCount();
		for (i = 0; i < rows; i++) {
			String s = tabel.getValueAt(i, cols - 1).toString();
			if (s.equals("0")) {
				continue;
			}
			for (j = 0; j < cols; j++) {

				System.out.print(tabel.getValueAt(i, j).toString() + " ");

			}
			int id_carte = Integer.parseInt(tabel.getValueAt(i, 0).toString());
			int nr_bucati = Integer.parseInt(tabel.getValueAt(i, 5).toString());

			list.add(new Comanda(id, idComClient, id_carte, nr_bucati));
			id++;

		}
		System.out.println(list);

		return list;
	}

	public void refreshTable() throws SQLException {
		JTable tabel = TableGenerator.createTableCatalog();
		putTable(tabel);

	}

	public String recordOrder() {
		String s = "";
		int i, j;
		int total = 0, nrObiecte = 0;
		int rows = tabel.getRowCount();
		int cols = tabel.getColumnCount();
		int excludeRow = 3; // nu vrem sa afisam stocul nostru pe bon
		s += "id---titlu---pret---gen\n";
		for (i = 0; i < rows; i++) {
			String checkBuc = tabel.getValueAt(i, cols - 1).toString();
			if (checkBuc.equals("0")) {
				continue;
			}
			nrObiecte++;
			s += nrObiecte + ". ";
			for (j = 0; j < cols; j++) {
				if (j == excludeRow)
					continue;
				s += tabel.getValueAt(i, j) + "---";

			}
			// System.out.println(s);
			total += Integer.parseInt(tabel.getValueAt(i, 2).toString());
			s += "\n";
		}
		s += "nr obiecte= " + nrObiecte + "\n";
		s += "total= " + total;
		return s;

	}

}
