package secondaryView;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;

import bill.Factura;
import mainView.View;

public class FacturaView extends View{
	private JButton finish=new JButton("finalizeaza comanda");
	JTable table; //aici pun comanda
	JTextArea area=new JTextArea(300,200);
	
	public FacturaView() {
		super("Factura comanda","factura.jpg",new Dimension(500,800));
		//screen.add(table);
		setFinish(buttonStyle(getFinish(),20,Color.white));
		getFinish().setBounds(100,600,300,50);
		getScreen().add(getFinish());
		
		JScrollPane js=new JScrollPane(area,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
				
		area.setEditable(false);
		js.setBounds(100,250,300,300);
		area.setFont(myFont);
		area.setText("sasa:");
		js.setVisible(true);
		getScreen().add(js);
	}


	public void scrieFactura(String s) {
		area.setText(s);
	}
	public JButton getFinish() {
		return finish;
	}

	public void setFinish(JButton finish) {
		this.finish = finish;
	}
}
