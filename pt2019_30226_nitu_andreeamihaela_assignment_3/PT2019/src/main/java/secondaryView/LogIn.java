package secondaryView;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import business.Functions;
import mainView.View;
import model.Persoana;

public class LogIn extends View {
	JTextField user = new JTextField(10);
	private JButton ok = new JButton("ok");
	public Persoana persoana=null;
	
	public LogIn() {
		super("Pagina principala", null, new Dimension(220, 200));
		Color c = new Color(241, 196, 15);
		setOk(buttonStyle(getOk(), 15, c));

		user.setEditable(true);
		JPanel jp = getBox("Nume Utilizator:", user);
		jp.setBounds(0, 10, 200, 300);
		jp.add(getOk());
		getScreen().add(jp);

		ok.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String name = user.getText();
				try {
					Persoana p=Functions.existsPerson(name);
					if (p!=null) {
						JOptionPane.showMessageDialog(null, "Autentificare realizata!");
						reset();
						disappearOnScreen();
						persoana=p;
					}
				} catch (Exception ea) {
					JOptionPane.showMessageDialog(null, "Nu exista acest user.Comunicati cu administratorul!");
					reset();
				}

			}
		});
	}

	public void reset() {
		user.setText("");
	}

	public JButton getOk() {
		return ok;
	}

	public void setOk(JButton ok) {
		this.ok = ok;
	}
}
