package dataBase;


import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;

import com.mysql.jdbc.Connection;

public class ConnectionBd {
	protected boolean status;
	private static ConnectionBd con=new ConnectionBd();
	
	  private ConnectionBd()
	    {
	        try{
	            Class.forName("dataBase.ConnectionBd");
	        } catch (ClassNotFoundException e) {
	            e.printStackTrace();
	        }
	    }
	  
	private  Connection createConnection() {
			try {
				return (Connection) DriverManager.getConnection("jdbc:mysql://localhost/librarie", "root", "");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return null;
		}
	  
	public static Connection getConnection() {
		return con.createConnection();
	}
	
	
	
	public static void close(java.sql.Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				System.out.println( "An error occured while trying to close the connection");
			}
	
		}
	}
	public static void close(Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				System.out.println(  "An error occured while trying to close the statement");
			}
		}
	}

	public static void close(ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				System.out.println("An error occured while trying to close the ResultSet");
			}
		}
	}
	}