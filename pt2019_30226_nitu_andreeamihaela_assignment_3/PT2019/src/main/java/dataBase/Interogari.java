package dataBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.*;

public class Interogari {
	private static String selPersoane = "select * from persoana";
	private static String selCarti = "select * from carti";
	private static String delCarti = "Delete from persoana where id=?";

	// de inchis mereu conexiunile!!!!

	public static ResultSet getResult() throws SQLException {
		Connection c = ConnectionBd.getConnection();
		PreparedStatement procStmt = c.prepareStatement(selPersoane);

		ResultSet rs;
		rs = procStmt.executeQuery();
		return rs;
	}

	public static int delClient(int id) throws SQLException {
		Connection c = ConnectionBd.getConnection();
		PreparedStatement st = c.prepareStatement(delCarti);
		st.setInt(1, id); // al catelea ?
		int k = st.executeUpdate();
		if (k == 0) // daca am sters ceva returneaza 1 altfel 0.
			return 0;
		return 1;

	}

	public static ArrayList<Persoana> selClients() throws SQLException {
		{
			Connection c = ConnectionBd.getConnection();
			PreparedStatement procStmt = c.prepareStatement("Select * from client");
			;
			ResultSet rs;
			rs = procStmt.executeQuery();
			ArrayList<Persoana> list = new ArrayList<Persoana>();
			while (rs.next()) {
				list.add(new Persoana(rs.getInt("id"), rs.getString("nume"), rs.getString("telefon"),
						rs.getString("strada")));
			}
			return list;

		}
	}

}
