package dataBase;

import java.util.ArrayList;

public class CreateStringQuerry {
	
	public static String createFindQuery(String simpleName,String field) {
		String s ="Select * from "+simpleName+" where " + field + " =?";
		return s;
	}
	public static String createUpdateQuery(String simpleName,ArrayList<String> fields,ArrayList<Object> inFields) {
		String s="update "+simpleName+" set ";
		int i;
		for(i=1;i<fields.size()-1;i++) {
			s+=fields.get(i)+"="+inFields.get(i)+",";
		}
		s+=fields.get(i)+"="+inFields.get(i);
		s+=" Where " + "id"+" =?";
		return s;
	}
	public static String createDeleteQuery(String simpleName,String field) {
		String s ="Delete from "+simpleName+" where " + field + " =?";
		return s;
	}
	public static String createSelectQuery(String simpleName,String field) {
		String s ="select * from "+simpleName+" where " + field + " =?";
		return s;
	}
	
	public static String createSelectAllQuery(String simpleName) {
		String s ="select * from "+simpleName;
		return s;
	}
	
	public static String createAddQuery(String simpleName,ArrayList<Object> list) {
		String s ="INSERT INTO "+simpleName+" VALUES(";
		int i;
		for(i=0;i<list.size()-1;i++) { //mergem pana la penultimul element din cauza virgulei
			s+=list.get(i)+",";
		}
		s+=list.get(i)+")";
		return s;
	}
	
	

}
