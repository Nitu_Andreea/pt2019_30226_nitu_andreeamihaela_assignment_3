package daoModel;

import javax.swing.JOptionPane;

import model.Carte;

public class CarteDao extends AbstractDao<Carte> {

	public int updateForComand(int id2, int nr_bucati) {
		Carte c = this.findById(id2);
		int ne=c.getNr_exemplare();
		if (c.getNr_exemplare() >= nr_bucati) {
			c.setNr_exemplare(c.getNr_exemplare() - nr_bucati);
			//if(c.getNr_exemplare()==0) {
			//	return 2;}
			//else 
			{
				c.setTitlu("'"+c.getTitlu()+"'");
				c.setGen("'"+c.getGen()+"'");
				this.update(id2,c);
			}
			}
		 
		else {
			JOptionPane.showMessageDialog(null,"Depasire stoc pentru carte cu id:"+c.getId()+"titlu: "+c.getTitlu());
			return 0;
		}
		return 1;
		
	}

	public boolean checkUpdateForComand(int id_carte, int nr_bucati) {
		Carte c = this.findById(id_carte);
		if (c.getNr_exemplare() >= nr_bucati)
			return true;
		return false;

	}

}
