package daoModel;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import business.Functions;
import dataBase.ConnectionBd;
import dataBase.CreateStringQuerry;

public class AbstractDao<T> {
	protected final Class<T> clasa;
//	private static String delCarti = "Delete from persoana where id=?";

	@SuppressWarnings("unchecked")
	public AbstractDao() {
		this.clasa = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	/**
	 * -----------------------------------------FIELDS--------------------------------------------------
	 **/
	public ArrayList<Object> getFields(Object object) {
		ArrayList<Object> list = new ArrayList<Object>();
		for (Field field : object.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			Object obj;
			try {
				obj = field.get(object);
				list.add(obj.toString());

			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}

		}
		return list;
	}

	public ArrayList<String> getNameFields(Object o) {
		ArrayList<String> list = new ArrayList<String>();
		System.out.println( o.getClass().getName());
		for (Field field : o.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			list.add(field.getName());
		}
		return list;
	}

	public List<T> findAll() {
		// TODO:
		return null;
	}

	/**
	 * -----------------------------------------FIND BY
	 * ID--------------------------------------------------
	 **/
	public T findById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		String query = CreateStringQuerry.createFindQuery(clasa.getSimpleName(), "id");
		try {
			connection = ConnectionBd.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			return createObjects(resultSet).get(0);
		} catch (SQLException e) {
			System.out.println(clasa.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionBd.close(resultSet);
			ConnectionBd.close(statement);
			ConnectionBd.close(connection);
		}
		return null;
	}

	/**
	 * -----------------------------------------Returns List of
	 * Obj--------------------------------------------------
	 **/
	protected List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();

		try {
			while (resultSet.next()) {
				T instance = clasa.newInstance();
				for (Field field : clasa.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), clasa);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * -----------------------------------------SELECT
	 * *--------------------------------------------------
	 **/
	public ArrayList<String> getAllSelected(Object obj) throws SQLException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		String query = CreateStringQuerry.createSelectAllQuery(clasa.getSimpleName());
		try {

			connection = ConnectionBd.getConnection();
			statement = connection.prepareStatement(query);
			 resultSet = statement.executeQuery();
			
			ArrayList<String> str=getNameFields(obj);
			ArrayList<String> list=new ArrayList<String>(); 
			
			while (resultSet.next()) {
				for (int i = 0; i < str.size(); i++) {
					list.add(resultSet.getString(str.get(i)));
				}
			}
			return list;
			
		} catch (SQLException e) {
			System.out.println(clasa.getName() + "DAO:select " + e.getMessage());
			JOptionPane.showMessageDialog(null, "Nu s-a putut realiza selectarea");
		} finally {
			ConnectionBd.close(resultSet);
			ConnectionBd.close(statement);
			ConnectionBd.close(connection);
			
		}
		return null;

	}
	/**
	 * -----------------------------------------delete--------------------------------------------------
	 **/
	public void deleteById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		String query = CreateStringQuerry.createDeleteQuery(clasa.getSimpleName(),"id");
		System.out.println(query);
		try {
			connection = ConnectionBd.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			statement.executeUpdate();

		} catch (SQLException e) {
			System.out.println(clasa.getName() + "DAO:delete " + e.getMessage());
			JOptionPane.showMessageDialog(null, "Nu s-a putut realiza stergerea");
		} finally {
			ConnectionBd.close(resultSet);
			ConnectionBd.close(statement);
			ConnectionBd.close(connection);
		}
	}

	/**
	 * -----------------------------------------insert--------------------------------------------------
	 **/
	public void insert(T t) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		ArrayList<Object> list = getFields(t);
		String query = CreateStringQuerry.createAddQuery(clasa.getSimpleName(), list);
		System.out.println(query);
		try {
			connection = ConnectionBd.getConnection();
			statement = connection.prepareStatement(query);
			statement.executeUpdate();

		} catch (SQLException e) {
			System.out.println(clasa.getName() + "DAO:insert" + e.getMessage());
			JOptionPane.showMessageDialog(null, "Nu s-a putut realiza inserarea");
		} finally {
			ConnectionBd.close(resultSet);
			ConnectionBd.close(statement);
			ConnectionBd.close(connection);
		}
	}
/**---------------------------------------------Update------------------------------------------------------*/
	public void update(int id,Object t) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		
		ArrayList<Object> inlist = getFields(t);
		ArrayList<String> list =getNameFields(t);
		String query = CreateStringQuerry.createUpdateQuery(clasa.getSimpleName(),list,inlist);
		System.out.println(query);
		try {
			connection = ConnectionBd.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			statement.executeUpdate();

		} catch (SQLException e) {
			System.out.println(clasa.getName() + "DAO:update " + e.getMessage());
			JOptionPane.showMessageDialog(null, "Nu s-a putut realiza update-ul");
		} finally {
			ConnectionBd.close(resultSet);
			ConnectionBd.close(statement);
			ConnectionBd.close(connection);
		}	
	}

}
