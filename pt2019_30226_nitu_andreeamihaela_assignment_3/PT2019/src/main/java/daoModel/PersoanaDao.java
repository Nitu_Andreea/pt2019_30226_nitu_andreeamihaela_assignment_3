package daoModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dataBase.ConnectionBd;
import dataBase.CreateStringQuerry;
import model.Persoana;

public class PersoanaDao extends AbstractDao<Persoana>{

	public Persoana findByName(String name) {
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		String query = CreateStringQuerry.createFindQuery(clasa.getSimpleName(), "nume");
		try {
			connection = ConnectionBd.getConnection();
			statement = connection.prepareStatement(query);
			statement.setString(1,name);
			resultSet = statement.executeQuery();

			return createObjects(resultSet).get(0);
		} catch (SQLException e) {
			System.out.println(clasa.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionBd.close(resultSet);
			ConnectionBd.close(statement);
			ConnectionBd.close(connection);
		}
		return null;
	}	
	
	}
