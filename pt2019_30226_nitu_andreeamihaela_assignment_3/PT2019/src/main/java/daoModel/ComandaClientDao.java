package daoModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dataBase.ConnectionBd;
import dataBase.CreateStringQuerry;
import model.ComandaClient;
import model.Persoana;

public class ComandaClientDao extends AbstractDao<ComandaClient>{
	private int id,id_client;


	public int getIdOfPerson() {
		
		return 0;
	}
	public int getIdOfPerson(Persoana p) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		String query = CreateStringQuerry.createFindQuery(clasa.getSimpleName(), "id_client");
		try {
			connection = ConnectionBd.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id_client);
			resultSet = statement.executeQuery();

			return resultSet.getInt("id");
		} catch (SQLException e) {
			System.out.println(clasa.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionBd.close(resultSet);
			ConnectionBd.close(statement);
			ConnectionBd.close(connection);
		}
		return -1;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_client() {
		return id_client;
	}

	public void setId_client(int id_client) {
		this.id_client = id_client;
	}

}
