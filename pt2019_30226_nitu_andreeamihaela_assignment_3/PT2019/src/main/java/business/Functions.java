package business;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import daoModel.*;
import dataBase.*;
import model.*;

public class Functions {
	static ComandaClientDao comandaClientDao = new ComandaClientDao();;
	static ComandaDao comandaDao = new ComandaDao();;
	static CarteDao carteDao = new CarteDao();;
	static PersoanaDao persoanaDao = new PersoanaDao();;

	/**
	 * -----------------------------------------Fields--------------------------------------------------
	 **/
	public static ArrayList<String> getFieldsComandaClient() {
		return comandaClientDao.getNameFields(new ComandaClient());
	}

	public static ArrayList<String> getFieldsComanda() {
		return comandaDao.getNameFields(new Comanda());
	}

	public static ArrayList<String> getFieldsCarte() {
		return carteDao.getNameFields(new Carte());
	}

	public static ArrayList<String> getFieldsPersoana() {
		return persoanaDao.getNameFields(new Persoana());
	}

	/**
	 * -----------------------------------------SELECT
	 * *--------------------------------------------------
	 **/

	public static ArrayList<String> getAllSelectedComandaClient() {
		ArrayList<String> list = null;
		try {
			list = comandaClientDao.getAllSelected(new ComandaClient());
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return list;
	}

	public static ArrayList<String> getAllSelectedComanda() {
		ArrayList<String> list = null;
		try {
			list = comandaDao.getAllSelected(new Comanda());
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return list;
	}

	public static ArrayList<String> getAllSelectedCarti() {
		ArrayList<String> list = null;
		try {
			list = carteDao.getAllSelected(new Carte());
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return list;
	}

	public static ArrayList<String> getAllSelectedPersoane() {
		ArrayList<String> list = null;
		try {
			list = persoanaDao.getAllSelected(new Persoana());
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return list;
	}

	/**
	 * -----------------------------------------SELECT
	 * *--------------------------------------------------
	 **/

	public ArrayList<Persoana> getAllClients() throws SQLException {
		return Interogari.selClients();
	}

	/**
	 * -----------------------------------------DELETE--------------------------------------------------
	 **/

	public static void delPersoana(int id) throws SQLException {
		persoanaDao.deleteById(id);
	}

	public static void delComanda(int id) throws SQLException {
		comandaDao.deleteById(id);
	}

	public static void delComandaClient(int id) throws SQLException {
		comandaClientDao.deleteById(id);
	}

	public static void delCarte(int id) throws SQLException {
		carteDao.deleteById(id);
	}

	/**
	 * -----------------------------------------update--------------------------------------------------
	 **/

	public static void updatePersoana(int id, Persoana p) {
		persoanaDao.update(id, p);
	}

	public static void updateComanda(int id, Comanda p) {
		comandaDao.update(id, p);
	}

	public static void updateComandaClient(int id, ComandaClient p) {
		comandaClientDao.update(id, p);
	}

	public static void updateCarte(int id, Carte p) {
		carteDao.update(id, p);
	}

	/**
	 * -----------------------------------------ADD--------------------------------------------------
	 **/

	public void addPersoana(Persoana p) {
		persoanaDao.insert(p);
	}

	public static ResultSet getRs() {
		try {
			return Interogari.getResult();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void adaugaCarteBd(Carte c) {
		carteDao.insert(c);

	}

	public static void adaugaPersoanaBd(Persoana p) {
		persoanaDao.insert(p);

	}

	public static void adaugaComandaBd(Comanda c) {
		comandaDao.insert(c);

	}

	public static void adaugaComandaClientBd(ComandaClient c) {
		comandaClientDao.insert(c);
		System.out.println("s-a inserat");
	}

	public static Carte findCarte(int i) {
		return carteDao.findById(i);
	}

	public static Comanda findComanda(int i) {
		return comandaDao.findById(i);
	}

	public static ComandaClient findComandaClient(int i) {
		return comandaClientDao.findById(i);
	}

	public static Persoana findPersoana(int i) {
		return persoanaDao.findById(i);
	}

	public static Persoana existsPerson(String name) {
		Persoana p = persoanaDao.findByName(name);
		return p;
	}

	public static int getIdComandaClientOfPerson(Persoana p) {
		return comandaClientDao.getIdOfPerson(p);
	}

	public static boolean executaComenzile(ArrayList<Comanda> list) {
		boolean ok = true;
		for (Comanda c : list) {// verifica inainte daca e comanda valida
			if (carteDao.checkUpdateForComand(c.getId_carte(), c.getNr_bucati()) == false) {
				ok = false;
				break;
			}

		}
		if(ok==true) {
			for (Comanda c : list) {// insereaza si actualizeaza stocul; verifica inainte daca e comanda valida
				int stare=carteDao.updateForComand(c.getId_carte(), c.getNr_bucati());
				if (stare== 1)
					comandaDao.insert(c);
				else if(stare==2) { //daca nu, e imposibil sa mai adaugam la comanda cv de am sters
					comandaDao.insert(c);
					carteDao.deleteById(c.getId_carte());
				}
			}
		}
		return ok;

	}
}
